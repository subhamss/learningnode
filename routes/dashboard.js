var express = require('express');
var router = express.Router();
const path = require('path');

router.get('/dashboard', function(req, res, next){
    res.sendFile(path.join(global.viewPath, 'Home.html'),{title: "Home"});
});

module.exports = router;