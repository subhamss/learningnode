var express = require('express');
var router = express.Router();
var connection = require("./dbConfig");

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('/login', function(req, res, next){
  var email = req.body.email;
  var password = req.body.password;
  connection.query('SELECT * FROM Users WHERE email = ? AND password = ?',[email,password], function(err, results, fields){
    if (!err) {
      console.log("err >> " + err);
      res.json({
        status: true,
        results: results,
        fields: fields
      });
    }else {
      console.log("err >> " + err);
      res.json({
        status: false
      });
    }
  });
});

router.post('/signup', function(req, res, next){
  var firstname = req.body.firstname !== "undefined" ? req.body.firstname: "";
  var lastname = req.body.lastname !== "undefined" ? req.body.lastname : "";
  var email = req.body.email !== "undefined" ? req.body.email: "";
  var password = req.body.password !== "undefined" ? req.body.password : "";
  var confirmpassword = req.body.confirmpassword !== "undefined" ? req.body.confirmpassword : "";

  var users = {
    "firstname" : firstname,
    "lastname": lastname,
    "email" : email,
    "password" : password,
    "createdDate" : new Date(),
    "updateDate" : new Date()
  }

  connection.query('INSERT INTO Users SET ?',users, function(err, results, fields){
    if (err){
      console.log("err >>" + err);
      res.json({
        status: "error",
      });
    }else {
      console.log("results >> " + results);
      console.log("feilds >> " + fields);
      res.json({
        status: "success"
      });
    }
  })
});





module.exports = router;
