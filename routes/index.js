var express = require('express');
var router = express.Router();
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Login-Sign Up' });
});

router.get('/forgot-password', function(req, res, next){
  res.sendFile(path.join(global.viewPath, 'forgot-password.html'), {title: 'Forgot password'});
});

module.exports = router;
