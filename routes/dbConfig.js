var mysql = require('mysql');
var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "fileupload"
});


connection.connect(function(err){
    if (err){
        console.log("Error while connecting to database");
    }else {
        console.log("Database Connected successfully");
    }
})

module.exports = connection;